//校园导游系统.cpp
#include <graphics.h>
#include <conio.h>
#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <math.h>
#include <fstream>
#include "Stack.h"
#include "functions.h"
#include "Graph_List.h"
#define WIDTH  (1050+(LEFTBORDER*2)) //界面宽度
#define HEIGHT  (600+TOPBORDER+LEFTBORDER) //界面高度
#define UP 72
#define DOWN 80
//#define LEFT 75
//#define RIGHT 77
#define BACKGROUND   "background.jpg" //背景图
#define BKCOLOR      RGB(240,240,240)    //原RGB(240,240,240)
#define SELECTCOLOR  RGB(255,0,0)    //原RED
#define TEXTCOLOR    RGB(7,102,198)//GREEN//   //原BLUE
#define MENUCOLOR    RGB(208,161,227)//RGB(77,197,131)//RGB(112,112,112)//RGB(117,75,144)//RGB(7,102,198)  //原GREEN
#define CheckCancel if(iscancel){return -1;} //如果点击取消按钮，返回主界面
using namespace std;
Graph_List graph; 
int choose;
int menunum = 11; //菜单选项数
int menutop1 = 10;
int menutop = (menutop1 + 3);
#define left (LEFTBORDER)
int menuwidth = ((WIDTH - 2 * LEFTBORDER) / (menunum));   
int menuheight = 30;  //设置界面宽度和高度
int GetChoose();
int FrontMenu(Graph_List& graph); //界面
int ShowAllVertex(Graph_List& graph); //显示所有结点
int ShowAllPath(Graph_List& graph); //显示所有路径
int printroad(int dx, int dy, int sx, int sy) { //显示路径
    LINESTYLE linestyle; //定义线的样式
    getlinestyle(&linestyle);// 获取当前画线样式
    setlinestyle(PS_SOLID, 3, NULL, 0);// 设置当前画线样式
    setlinecolor(BLUE);	// 设置当前线条颜色
    line(sx, sy, dx, dy); //画线
    setlinestyle(&linestyle);// 设置当前画线样式
    double x1 = (sx + dx) / 2;
    double y1 = (sy + dy) / 2;
    double x2 = ((3.0 / 8.0) * sx + (5.0 / 8.0) * dx);
    double y2 = ((3.0 / 8.0) * sy + (5.0 / 8.0) * dy);
    //double x1=sx,x2=(sx+dx)/2,y1=sy,y2=(sy+dy)/2;
    double k1 = (y2 - y1) / (x2 - x1);
    double k2 = (-1) / k1;
    double delta = 1600; //=(y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
    double a = sqrt((delta / 16) / (1 + k2 * k2));
    double b = a * fabs(k2);
    if (((x1 > x2) && (y1 < y2)) || ((x1 < x2) && (y1 > y2))) {
    }
    else {
        b = -1 * b;
    }
    if (x1 == x2) {
        a = 5;
        b = 0;
    }
    if (y1 == y2) {
        a = 0;
        b = 3;
    }
    POINT pts[] = { {x1, y1}, {x2 + a, y2 + b}, {(x1 + x2) / 2, (y1 + y2) / 2}, };
    POINT pts1[] = { {x1, y1}, {x2 * 2 - (x2 + a), y2 * 2 - (y2 + b)}, {(x1 + x2) / 2, (y1 + y2) / 2}, }; //坐标
    setfillcolor(BLUE);// 设置当前填充颜色
    solidpolygon(pts1, 3);// 画填充的多边形(无边框)
    solidpolygon(pts, 3);// 画填充的多边形(无边框)
    return 0;
}
int GetMouseXY(int& mousex, int& mousey) { //定位鼠标的坐标，设置景点
    MOUSEMSG temp;
    temp.mkLButton = false;// 鼠标左键是否按下
    bool kick = false;
    while (!kick) {
        // if(MouseHit()) {
        temp = GetMouseMsg();// 获取一个鼠标消息。如果没有，就等待
        FlushMouseMsgBuffer();// 清空鼠标消息缓冲区
        if (temp.mkLButton == false) {// 鼠标左键是否按下
            mousex = temp.x;// 当前鼠标 x 坐标
            mousey = temp.y;// 当前鼠标 y 坐标
        }
        else {
            kick = temp.mkLButton;
        }
        // }
    }
    return 0;
}
int AddVertex(Graph_List& graph) { //添加景点
    LOGFONT font;
    gettextstyle(&font);// 获取当前字体样式
    settextstyle(10, 0, _T("宋体"));// 设置当前字体样式
    BeginBatchDraw();// 开始批量绘制
    setlinecolor(GREEN);// 设置当前线条颜色
    for (int lx = LEFTBORDER; lx <= WIDTH; lx += 50) {
        //outtextxy(lx, TOPBORDER - 10, &int_to_str(lx - LEFTBORDER)[0]);
        line(lx, TOPBORDER, lx, HEIGHT - LEFTBORDER);// 画竖线
    }
    for (int ly = TOPBORDER; ly <= HEIGHT - LEFTBORDER; ly += 50) {
        //outtextxy(10, ly - 5, &int_to_str(ly - TOPBORDER)[0]);
        line(LEFTBORDER, ly, WIDTH - LEFTBORDER, ly);// 画横线
    }
    FlushBatchDraw();// 执行未完成的绘制任务
    setlinecolor(MENUCOLOR);// 设置当前线条颜色
    settextstyle(&font);// 设置当前字体样式
    ::MessageBox(GetHWnd(), (LPCTSTR)"请在您要添加景点的位置点击", (LPCTSTR)"添加景点", MB_OK);// 弹出窗口
    int mousex, mousey;
    GetMouseXY(mousex, mousey);//定位鼠标的坐标，设置景点
    char  name[30];
    bool iscancel = false;
    //bool InputBox(LPTSTR pString, int nMaxCount, LPCTSTR pPrompt = NULL, LPCTSTR pTitle = NULL, LPCTSTR pDefault = NULL, int width = 0, int height = 0, bool bOnlyOK = true);

    iscancel = !InputBox((LPTSTR)name, 30, (LPCTSTR)"输入名称", (LPCTSTR)"添加景点", (LPCTSTR)"0", 0, 0, 0);// 获取用户输入
    CheckCancel
        char  describe[1000];
    iscancel = !InputBox(LPTSTR (&(describe[0])), 1000, (LPCTSTR)"输入简介：(按Ctrl+Enter确认输入)", (LPCTSTR)"添加景点", (LPCTSTR)"0", 0, 10, 0);// 获取用户输入
    CheckCancel
        char mark[10];
    int marki;
    while (1) {
        iscancel = !InputBox(LPTSTR (&(mark[0])), 10, (LPCTSTR)"输入代号", (LPCTSTR)"添加景点", 0, 0, 0, 0);// 获取用户输入
        CheckCancel
            marki = str_to_num(mark);//转换代号类型
        if (marki > 0 && graph.GetIndex(marki) == -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"此代号已存在，请重输", (LPCTSTR)"添加景点", MB_OK);// 弹出窗口
    }
    string namestr = "", describestr = "";
    graph.InsertVertex((string)(namestr + name), (string)(describestr + describe), marki, mousex, mousey);//把数据插入到结点
    ::MessageBox(GetHWnd(), (LPCTSTR)"添加成功", (LPCTSTR)"添加景点", MB_OK);
    return 0;
}
int AddPath(Graph_List& graph) { //添加边
    bool iscancel = false;
    int v1, v2;
    string source = "0000000000", destination = "0000000000", weightstr = "0000000000";
    while (1) {
        //bool InputBox(LPTSTR pString, int nMaxCount, LPCTSTR pPrompt = NULL, LPCTSTR pTitle = NULL, LPCTSTR pDefault = NULL, int width = 0, int height = 0, bool bOnlyOK = true);
        iscancel = !InputBox(LPTSTR (&(source[0])), 10, (LPCTSTR)"输入起点代号", (LPCTSTR)"添加边", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v1 = str_to_num(source);
        if (graph.GetIndex(v1) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"添加边", MB_OK);
    }
    while (1) {
        iscancel = !InputBox(LPTSTR (&(destination[0])), 10, (LPCTSTR)"输入终点代号", (LPCTSTR)"添加边", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v2 = str_to_num(destination);
        if (graph.GetIndex(v2) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"添加边", MB_OK);
    }
    double dx = graph.GetHead()[graph.GetIndex(v2)].GetX();
    double sx = graph.GetHead()[graph.GetIndex(v1)].GetX();
    double dy = graph.GetHead()[graph.GetIndex(v2)].GetY();
    double sy = graph.GetHead()[graph.GetIndex(v1)].GetY();
    double weight = (double)sqrt(((dx - sx) * (dx - sx) + (dy - sy) * (dy - sy)) / 30000.0) * 300.0;
    int retop = graph.InsertEdge(graph.GetIndex(v1), graph.GetIndex(v2), weight);
    if (retop == -3) { ::MessageBox(GetHWnd(), (LPCTSTR)"起点与终点相同！", (LPCTSTR)"添加边", MB_OK); return -3; }
    if (retop == -4) { ::MessageBox(GetHWnd(), (LPCTSTR)"边已存在！", (LPCTSTR)"添加边", MB_OK); return -4; }
    ::MessageBox(GetHWnd(), (LPCTSTR)"添加成功", (LPCTSTR)"添加边", MB_OK);
    return 0;
}
int DeleteVertex(Graph_List& graph) { //删除景点
    bool iscancel = false;
    char v[10];
    int v1;
    while (1) {
        iscancel = !InputBox(LPTSTR (&(v[0])), 10, (LPCTSTR)"输入代号", (LPCTSTR)"删除景点", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v1 = str_to_num(v);

        if (graph.GetIndex(v1) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"此代号不存在，请重输", (LPCTSTR)"删除景点", MB_OK);
    }
    graph.DeleteVertex(graph.GetIndex(v1));
    return 0;
}
int DeletePath(Graph_List& graph) { // 删除边
    bool iscancel = false;
    int v1, v2;
    string source = "0000000000", destination = "0000000000";
    while (1) {
        iscancel = !InputBox(LPTSTR (&(source[0])), 10, (LPCTSTR)"输入起点代号", (LPCTSTR)"删除边", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v1 = str_to_num(source);
        if (graph.GetIndex(v1) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"删除边", MB_OK);
    }
    while (1) {
        iscancel = !InputBox(LPTSTR (&(destination[0])), 10, (LPCTSTR)"输入终点代号", (LPCTSTR)"删除边", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v2 = str_to_num(destination);
        if (graph.GetIndex(v2) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"删除边", MB_OK);
    }
    int retop = graph.DeleteEdge(graph.GetIndex(v1), graph.GetIndex(v2));
    if (retop == -1) { ::MessageBox(GetHWnd(), (LPCTSTR)"边不存在！", (LPCTSTR)"删除边", MB_OK); return -1; }

    ::MessageBox(GetHWnd(), (LPCTSTR)"删除成功", (LPCTSTR)"删除边", MB_OK);
    return 0;
}
int DrawShortestPath(Graph_List& graph, int v1, int v2) {  //画出最短路径
    Vertex* Head = graph.GetHead();

    int* path = new int[graph.NumberOfVertices()];
    graph.DShortestPath(v1, path);
    LStack<int > stack;
    
    if (path[v2] == -1) {
        ::MessageBox(GetHWnd(), (LPCTSTR)"*** 无路径 ***", (LPCTSTR)"查询路径", MB_OK);
        return -1;
    }
    int i = v2;
    while (i != path[v1]) {
        stack.Push(i);
        i = path[i];
    }
    delete[]path;
   
    int temp = v1, temppre = v1, length = 0;
    string pathstr, tempstr;
    BeginBatchDraw();// 开始批量绘制
    while (!stack.IsEmpty()) {
        stack.Pop(temp);
        printroad(Head[temppre].GetX(), Head[temppre].GetY(), Head[temp].GetX(), Head[temp].GetY());//显示路径
        int pathweight = graph.GetWeight(temppre, temp);
        length += pathweight;
        tempstr = tempstr + "从" + int_to_str(Head[temppre].GetMark()) + "到" + int_to_str(Head[temp].GetMark()) + " : " + int_to_str(pathweight) + " 米" + "\n";
        temppre = temp;
    }

    FlushBatchDraw();// 执行未完成的绘制任务
    setbkcolor(BKCOLOR);// 显示颜色
    settextcolor(TEXTCOLOR);//显示颜色
    string str = "最短路径长度为：";
    str = str + int_to_str(length) + " 米\n" + "步行所需时间：" + int_to_str(length / 90) + "分钟\n";
    str += tempstr;
    ::MessageBox(GetHWnd(), LPTSTR (&str[0]), (LPCTSTR)"查询路径", MB_OK);
    return 0;
}
int FindPath(Graph_List& graph) { //查找路径
    bool iscancel = false;
    int v1, v2;
    string source = "0000000000", destination = "0000000000";
    while (1) {
        iscancel = !InputBox(LPTSTR (&(source[0])), 10, (LPCTSTR)"输入起点代号", (LPCTSTR)"查询路径", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v1 = str_to_num(source);
        if (graph.GetIndex(v1) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"查询路径", MB_OK);
    }
    while (1) {
        iscancel = !InputBox(LPTSTR (&(destination[0])), 10, (LPCTSTR)"输入终点代号", (LPCTSTR)"查询路径", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v2 = str_to_num(destination);
        if (graph.GetIndex(v2) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"查询路径", MB_OK);
    }
    DrawShortestPath(graph, graph.GetIndex(v1), graph.GetIndex(v2));
    return 0;
}
void ShowVertex(Graph_List& graph, int i) { //查询景点
    Vertex* Head = graph.GetHead();
    if (i < 0 || i > graph.NumberOfVertices() - 1) {
        ::MessageBox(GetHWnd(), (LPCTSTR)"查询位置不存在", (LPCTSTR)"景点查询", MB_OK);   //GetHWnd()获得窗口句柄
        return;
    }
    string str = "代号：" + int_to_str(Head[i].GetMark()) + "\n" + "简介: " + Head[i].GetDescribe();
    str = "名称：" + Head[i].GetVerName() + "\n" + str;
    ::MessageBox(GetHWnd(), LPTSTR (&str[0]), (LPCTSTR)"景点查询", MB_OK);   //GetHWnd()获得窗口句柄
}
int ModifyMenu() { //修改菜单
    int choose1 = 1;//定位选择的选项
    char temp = 0, key;
    while (temp != '\r') {
        if (_kbhit()) { //检查当前是否有键盘输入
            key = _getch();//读取字符
            fflush(stdin);//清除读写缓冲区
            switch (key) {
            case UP: {
                choose1--;
            };
                   break;
            case DOWN: {
                choose1++;
            };
            }
        }
        if (choose1 == 6) {
            choose1 = 5;
        }
        if (choose1 == 0) {
            choose1 = 1;
        }
        cleardevice();// 清屏
        outtextxy((WIDTH / 2) - 80, 120, (LPCTSTR )"修改景点");// 在指定位置输出字符串
        outtextxy((WIDTH / 2) - 180, 230, (LPCTSTR)"按↑和↓选择选项");
        outtextxy((WIDTH / 2) - 180, 250, (LPCTSTR)"按ENTER键确定选择");
        outtextxy(300, 300, (LPCTSTR)"请选择选项：");
        outtextxy(420, 350, (LPCTSTR)"修改名称");
        outtextxy(420, 380, (LPCTSTR)"修改简介");
        outtextxy(420, 410, (LPCTSTR)"修改代号");
        outtextxy(420, 440, (LPCTSTR)"修改坐标");
        outtextxy(420, 470, (LPCTSTR)"返回");
        outtextxy(390, 350 + (choose1 - 1) * 30, (LPCTSTR)"→");
        FlushBatchDraw();// 执行未完成的绘制任务
        temp = _getch();
    }
    return choose1;
}
int ModifyVertex(Graph_List& graph) { //修改景点
    bool iscancel = false;
    int v1;
    char v1str[10] = { 'a' };
    while (1) {
        iscancel = !InputBox(LPTSTR (&(v1str[0])), 10, (LPCTSTR)"输入代号", (LPCTSTR)"修改景点", (LPCTSTR)"0", 0, 0, 0);
        CheckCancel
            v1 = str_to_num(v1str);
        if (graph.GetIndex(v1) != -1) {
            break;
        }
        ::MessageBox(GetHWnd(), (LPCTSTR)"该位置不存在，请重输", (LPCTSTR)"修改景点", MB_OK);
    }
    int v = graph.GetIndex(v1);
    Vertex* Head = graph.GetHead();
    string name = Head[v].GetVerName();
    string describe = Head[v].GetDescribe();
    int marki = Head[v].GetMark(), x = Head[v].GetX() - LEFTBORDER, y = Head[v].GetY() - TOPBORDER;
    bool isreturn = false;
    while (!isreturn) {
        int in = ModifyMenu();
        switch (in) {
        case 1: {
            iscancel = !InputBox(LPTSTR (&(name[0])), 30, (LPCTSTR)"输入名称", (LPCTSTR)"修改景点", (LPCTSTR)"0", 0, 0, 0);
            CheckCancel
        };
              break;
        case 2: {
            iscancel = !InputBox(LPTSTR (&(describe[0])), 1000, (LPCTSTR)"输入简介：(按Ctrl+Enter确认输入)", (LPCTSTR)"修改景点", (LPCTSTR)"0", 0, 10, 0);
            CheckCancel
        };
              break;
        case 3: {
            char mark[10];
            while (1) {
                iscancel = !InputBox(LPTSTR (&(mark[0])), 10, (LPCTSTR)"输入代号", (LPCTSTR)"修改景点", 0, 0, 0, 0);
                CheckCancel
                    marki = str_to_num(mark);
                if (marki > 0 && (marki == Head[v].GetMark() || graph.GetIndex(marki) == -1)) {
                    break;
                }
                ::MessageBox(GetHWnd(), (LPCTSTR)"输入有误或代号已存在，请重输", (LPCTSTR)"修改景点", MB_OK);
            }
        };
              break;
        case 4: {
            char xstr[10], ystr[10];
            while (1) {
                iscancel = !InputBox(LPTSTR (&(xstr[0])), 10, (LPCTSTR)"输入X坐标", (LPCTSTR)"修改景点", (LPCTSTR)"0", 0, 0, 0);
                CheckCancel
                    x = str_to_num(xstr);
                if (x > 0 && x < WIDTH) {
                    break;
                }
                ::MessageBox(GetHWnd(), (LPCTSTR)"您所输入的坐标不在图像范围内", (LPCTSTR)"修改景点", MB_OK);
            }
            while (1) {
                iscancel = !InputBox(LPTSTR (&(ystr[0])), 10, (LPCTSTR)"输入Y坐标", (LPCTSTR)"修改景点", (LPCTSTR)"0", 0, 0, 0);
                CheckCancel
                    y = str_to_num(ystr);
                if (y > 0 && y < HEIGHT) {
                    break;
                }
                ::MessageBox(GetHWnd(), (LPCTSTR)"您所输入的坐标不在图像范围内", (LPCTSTR)"修改景点", MB_OK);
            }
        };
              break;
        case 5: {
            graph.ModifyVertex(v, name, describe, marki, x, y);
            ShowVertex(graph, v);
            isreturn = true;
        };
              break;
        default:
        {};
        break;
        }
    }
    return 0;
}
int ShowAllVertex(Graph_List& graph) { //显示结点
    Vertex* Head = graph.GetHead(); //创建head指针指向结点列表
    //putimage(LEFTBORDER, TOPBORDER, graph.GetMap());
    setbkcolor(RGB(240, 240, 240));//绘图背景色
    settextcolor(RED);//设置文件颜色
    setfillcolor(RGB(255, 0, 0));//设置填充颜色
    LOGFONT font1;
    gettextstyle(&font1);
    settextstyle(font1.lfHeight - 2, 0, _T("宋体"));//获取和设置字体样式
    for (int i = 0; i <= graph.NumberOfVertices() - 1; i++) {
        int x = Head[i].GetX(), y = Head[i].GetY(); //遍历获取x，y轴参数
        string str = "";
        str += int_to_str(Head[i].GetMark()); //在地图上显示名称
        if (Head[i].GetVerName() != "*") 
            str = str + " " + Head[i].GetVerName(); //如果名称不为*则显示完整的代号+名称
        solidcircle(x, y, 4); //显示结点
        outtextxy(x, y, (LPCTSTR)&str[0]); //输出str字符串
    }
    settextstyle(&font1); //字体样式
    FlushBatchDraw(); //完成绘画
    return 0;
}
int ShowAllPath(Graph_List& graph) { //显示路径
    LINESTYLE linestyle; //边线
    getlinestyle(&linestyle);
    setlinestyle(PS_SOLID, 3, NULL, 0); //获取和设置边的样式
    setlinecolor(BLUE); 
    for (int i = 0; i <= graph.NumberOfVertices() - 1; i++) {
        Edge* p = graph.GetHead()[i].Getadj();
        while (p != NULL) {
            line(graph.GetHead()[i].GetX(), graph.GetHead()[i].GetY(), graph.GetHead()[p->GetVerAdj()].GetX(), graph.GetHead()[p->GetVerAdj()].GetY()); //画出当前结点与相邻结点的边
            p = p->Getlink();  //指向下一个邻边
        }
    } //遍历画出所有边
    setlinestyle(&linestyle); //边的样式
    FlushBatchDraw(); //完成绘画
    return  0;
}
int FindMenu() { //查找菜单栏
    int choose1 = 1;
    char temp = 0, key;
    while (temp != '\r') {
        if (_kbhit()) {
            key = _getch();
            fflush(stdin);
            switch (key) {
            case UP: {
                choose1--;
            };
                   break;
            case DOWN: {
                choose1++;
            };
            }
        }
        if (choose1 == 4) {
            choose1 = 3;
        }
        if (choose1 == 0) {
            choose1 = 1;
        }
        cleardevice();
        outtextxy((WIDTH / 2) - 80, 120, (LPCTSTR)"查询景点");//在指定位置输出字符串
        outtextxy((WIDTH / 2) - 180, 230, (LPCTSTR)"按↑和↓选择选项");
        outtextxy((WIDTH / 2) - 180, 250, (LPCTSTR)"按ENTER键确定选择");
        outtextxy(300, 300, (LPCTSTR)"请选择选项：");
        outtextxy(420, 350, (LPCTSTR)"按代号查询");
        outtextxy(420, 380, (LPCTSTR)"按名称查询");
        outtextxy(420, 410, (LPCTSTR)"返回");
        outtextxy(390, 350 + (choose1 - 1) * 30, (LPCTSTR)"→");
        FlushBatchDraw();
        temp = _getch();
    }
    return choose1;
}
int FindVertex(Graph_List& graph) { //查找景点
    bool iscancel = false;
    bool isreturn = false;
    while (!isreturn) {
        int in = FindMenu();
        switch (in) {
        case 1: {
            char v1str[10] = { 'a' };
            int v1;
            iscancel = !InputBox(LPTSTR (&(v1str[0])), 10, (LPCTSTR)"输入代号", (LPCTSTR)"查询景点", (LPCTSTR)"0", 0, 0, 0);
            CheckCancel
                v1 = str_to_num(v1str);

            ShowVertex(graph, graph.GetIndex(v1));
        };
              break;
        case 2: {
            char v1str[10] = { 'a' };
            string v1;
            iscancel = !InputBox(LPTSTR (&(v1str[0])), 10, (LPCTSTR)"输入名称", (LPCTSTR)"查询景点", (LPCTSTR)"0", 0, 0, 0);
            CheckCancel
                v1 = "";
            v1 += v1str;
            ShowVertex(graph, graph.GetIndex(v1));
        };
              break;
        case 3: {

            isreturn = true;
        };
              break;
        default:
        {};
        break;
        }
    }
    return 0;
}
int Open(Graph_List& graph, bool isfirstopen) { //打开文件
    fstream filestr;
    char mapname[256] = { 'a' };//mapname保存文件名
    if (isfirstopen) {
        filestr.open("map.txt");
        strcpy_s(mapname, "map.jpg");
    }
    else {
        bool iscancel = false;
        char filename[256] = { 'a' };
        while (1) {
            iscancel = !InputBox((LPTSTR)&(filename[0]), 10, (LPCTSTR)"输入文本文件名(不需输'.txt')", (LPCTSTR)"打开", (LPCTSTR)"0", 0, 0, 0);
            CheckCancel
                strcat_s(filename, ".txt");
            filestr.open(filename);//打开文件
            if (filestr.is_open()) {
                break;
            }
            ::MessageBox(GetHWnd(), (LPCTSTR)"无此文件，请重输", (LPCTSTR)"打开", MB_OK); //提示框
        }
        while (1) {
            iscancel = !InputBox((LPTSTR) &(mapname[0]), 10, (LPCTSTR)"输入图片文件名(不需输'.jpg')", (LPCTSTR)"打开", (LPCTSTR)"0", 0, 0, 0);
            CheckCancel
                strcat_s(mapname, ".jpg");
            if ((_access(mapname, 0)) != -1) { //_access,判断文件是否存在,如果文件具有指定的访问权限，则函数返回0；如果文件不存在或者不能访问指定的权限，则返回-1.
                break;
            }
            ::MessageBox(GetHWnd(), (LPCTSTR)"无此文件，请重输", (LPCTSTR)"打开", MB_OK);
        }
    }
    graph.graph_con(filestr, mapname);//从文件中提取结点和边信息绘制图
    filestr.close();//释放文件流
    putimage(LEFTBORDER, TOPBORDER, graph.GetMap()); //绘制地图
    return 0;
}
int Save(Graph_List& graph) { //保存文件
    bool iscancel = false;
    Vertex* Head = graph.GetHead();
    char filename[256] = { 'a' };
    iscancel = !InputBox((LPTSTR)&(filename[0]), 10, (LPCTSTR)"输入文本文件名(不需输'.txt')", (LPCTSTR)"保存", (LPCTSTR)"0", 0, 0, 0);
    CheckCancel
        strcat_s(filename, ".txt");
    graph.SaveFile(filename);
    ::MessageBox(GetHWnd(), (LPCTSTR)"保存成功", (LPCTSTR)"保存", MB_OK);
    return 0;
}
int GetChoose() { 
    FlushMouseMsgBuffer();// 清空鼠标消息缓冲区
    MOUSEMSG temp;
    temp.mkLButton = false;
    bool kick = false;
    while (!kick) {
        // if(MouseHit()) {
        temp = GetMouseMsg();// 获取一个鼠标消息。如果没有，就等待
        FlushMouseMsgBuffer(); //清空鼠标消息缓冲区
        if (temp.mkLButton == false) {
        }
        else {
            if (temp.y < menutop + menuheight && temp.y > menutop) {
                kick = true;
            }
        }
        // }
    }
    choose = (temp.x - left) / menuwidth;
    return 0;
}
int FrontMenu(Graph_List& graph) { //菜单
    fflush(stdin);
    settextcolor(TEXTCOLOR);
    setfillcolor(BKCOLOR);
    solidrectangle(5, TOPBORDER, WIDTH, HEIGHT); //以背景色刷新
    setfillcolor(MENUCOLOR);
    solidrectangle(5, 0, WIDTH - 5, menutop1); //画最上方的绿色条
    putimage(LEFTBORDER, TOPBORDER, graph.GetMap());
    setfillcolor(BKCOLOR);
    solidrectangle(LEFTBORDER, menutop + textheight('a'), WIDTH - LEFTBORDER, menutop + textheight('a') + 5); //去掉选中的菜单下方的小绿块
    setfillcolor(TEXTCOLOR);
    setfillcolor(MENUCOLOR);
    solidrectangle(choose * menuwidth + left, menutop + textheight('a'), (choose + 1) * menuwidth + left, menutop + textheight('a') + 5); //画选中的菜单下方的小绿块
    roundrect(5, menutop + textheight('a') + 5, WIDTH - 5, HEIGHT - 5, 5, 3); //画图片周围的绿色边框
    solidrectangle(5, menutop + textheight('a') + 5, WIDTH - 5, menutop + textheight('a') + 10); //画图片上方与小绿块相连的绿色条
    setfillcolor(TEXTCOLOR);
    LOGFONT font1;
    gettextstyle(&font1);
    settextstyle(font1.lfHeight, 0, _T("宋体"));
    string menu[11] = { "     打开", "   显示", " 查询路径", " 查询景点", " 修改景点", " 添加景点", " 删除景点", "  添加边", "  删除边","   保存", "     退出" };//菜单栏
    for (int index = 0; index <= menunum - 1; index++) {
        if (index == choose)settextcolor(SELECTCOLOR);
        const char* p = menu[index].c_str();
        outtextxy(left + index * menuwidth, menutop, (LPCTSTR)p);
        if (index == choose)settextcolor(TEXTCOLOR);
    }
    settextstyle(&font1);
    setlinecolor(MENUCOLOR);
    for (int index1 = 0; index1 <= menunum + 1; index1++) {
        line(left + index1 * menuwidth, menutop, left + index1 * menuwidth, menutop + textheight('a'));
    }
    LOGFONT font;
    gettextstyle(&font);
    settextstyle(10, 0, _T("宋体"));
    settextcolor(TEXTCOLOR);
    for (int lx = LEFTBORDER; lx <= WIDTH; lx += 50) {
        outtextxy(lx, TOPBORDER - 10, (LPCTSTR)&int_to_str(lx - LEFTBORDER)[0]);
    }
    for (int ly = TOPBORDER; ly <= HEIGHT - LEFTBORDER; ly += 50) {
        outtextxy(10, ly - 5, (LPCTSTR)&int_to_str(ly - TOPBORDER)[0]);
    }
    settextstyle(&font);
    settextcolor(TEXTCOLOR);
    FlushBatchDraw();
    setbkcolor(BKCOLOR);
    settextcolor(TEXTCOLOR);
    return choose;
}
int main() {
    graph;
    initgraph(WIDTH, HEIGHT); //界面大小
    setbkcolor(BKCOLOR);
    cleardevice();// 清屏
    bool isfirstopen = true;
    while (1) {
        FrontMenu(graph);
        if (isfirstopen) {
            choose = 0;
        }
        else {
            GetChoose();
        }
        FrontMenu(graph);
        switch (choose) {
        case 0: {
            Open(graph, isfirstopen);
            isfirstopen = false;
        }
              break;
        case 1: {
            ShowAllPath(graph);
            ShowAllVertex(graph);
            int vertexnum = graph.NumberOfVertices(), edgenum = graph.NumberOfEdges();
            string numofvertices;
            numofvertices = "图中共有景点 " + int_to_str(vertexnum) + " 个\n" + "共有边 " + int_to_str(edgenum) + " 条\n";
            ::MessageBox(GetHWnd(), LPCSTR(&numofvertices[0]), (LPCSTR)"显示", MB_OK);
        }
              break;
        case 2: {
            ShowAllVertex(graph);
            FindPath(graph);
            ShowAllVertex(graph);
        };
              break;
        case 3: {
            ShowAllVertex(graph);
            FindVertex(graph);
        };
              break;
        case 4: {
            ShowAllVertex(graph);
            ModifyVertex(graph);
        };
              break;
        case 5: {
            ShowAllVertex(graph);
            AddVertex(graph);
        };
              break;
        case 6: {
            ShowAllVertex(graph);
            DeleteVertex(graph);
        };
              break;
        case 7: {
            ShowAllPath(graph);
            ShowAllVertex(graph);
            AddPath(graph);
        }
              break;
        case 8: {
            ShowAllPath(graph);
            ShowAllVertex(graph);
            DeletePath(graph);
        }
              break;
        case 9: {
            Save(graph);
        }
              break;
        case 10: {
            WinExec((LPCSTR)_T("taskkill /f /im conhost.exe /t"), 1);
            exit(0);//关闭界面
        }
               break;
        default: {
        }
               break;
        }
    }
    return 0;
}