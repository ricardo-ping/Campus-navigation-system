#ifndef   _MYStack_h_
#define   _MYStack_h_
template<class TL>
class LStack;
//结点定义
template<class TL>
class LStackNode
{
private:
    TL data;
    LStackNode<TL>* next;
public:
    friend class LStack<TL>;
    LStackNode()
    {
        next = NULL;
    }
    LStackNode(TL item, LStackNode<TL>* nextnode = NULL)
    {
        data = item;
        next = nextnode;
    }
    LStackNode(LStackNode<TL>& node)
    {
        data = node.data;    
        next = node.next;
    }
};
//栈定义
template<class TL>
class LStack
{
private:
    LStackNode<TL>* top;
public:
    LStack()
    {
        top = NULL;
    }
    bool Push(const TL& item)
    {
        LStackNode<TL>* p = top;
        top = new LStackNode<TL>;
        top->data = item;
        top->next = p;
        return true;
    }
    bool Pop(TL& item)
    {
        if (top == NULL)
        {
            return false;
        }
        else
        {
            item = top->data;
            LStackNode<TL>* p = top;
            top = top->next;
            delete p;
        }
        return true;
    }
    bool Peek(TL& item)
    {
        if (top == NULL)
        {
            return false;
        }
        else
        {
            item = top->data;
        }
    }
    ~LStack()
    {
        LStackNode<TL>* p = top;
        while (top != NULL)
        {
            top = top->next;
            delete p;
            p = top;
        }
        top = p = NULL;
    }
    bool IsEmpty()
    {
        return top == NULL;
    }
};
#endif  
#pragma once
