#ifndef  _MYGraph_List_h_
#define  _MYGraph_List_h_
#define LEFTBORDER 30 //左右边界
#define TOPBORDER 50 //上边界
#include<string>
#include <fstream>
#include <graphics.h>

class Edge
{
private:
    int VerAdj; //链接顶点序列，从0开始编号
    int cost; //边的权值
    Edge* link;//指向下一个邻边的指针
public:
    Edge()
    {
        VerAdj = -1;
        cost = -1;
        link = NULL;
    }
    Edge(int VerAdji, int costi, Edge* linki) : VerAdj(VerAdji), cost(costi), link(linki)  //初始化
    {
    }
    int& GetVerAdj()
    {
        return VerAdj;
    }
    int& Getcost()
    {
        return cost;
    }
    Edge*& Getlink()
    {
        return link;
    }
}; //定义边类
class Vertex
{
private:
    string VerName; //结点名称
    string Describe;//描述
    Edge* adjacent;//指向邻边的指针
    int mark; //代号
    int x;
    int y; //坐标
public:
    Vertex()
    {
        VerName = "*";
        Describe = "*";
        adjacent = NULL;
        mark = -1;
        x = -1;
        y = -1;
    } //初始化
    string& GetVerName()
    {
        return VerName;
    }
    string& GetDescribe()
    {
        return Describe;
    }
    Edge*& Getadj()
    {
        return adjacent;
    }
    int& GetMark()
    {
        return mark;
    }
    int& GetX()
    {
        return x;
    }
    int& GetY()
    {
        return y;
    }
};//定义结点类
class Graph_List
{
private:
    Vertex* Head;//结点链表的头指针
    int graphsize;//图中当前结点的个数
    int MaxCost; //最大值
    IMAGE* map;
    int InsertEdge_Do(const int& v1, const int& v2, int weight)   //插入时尽量保证顺序排列,插入边
    {
        if (Head[v1].Getadj() == NULL) {
            Head[v1].Getadj() = new Edge(v2, weight, NULL);  //若邻接表为空 直接在adjacent后添加边
            return 0;
        }
        Edge* p = Head[v1].Getadj();
        Edge* pt = p;
        while ((p != NULL) && (p->GetVerAdj() < v2)) {
            pt = p;
            p = p->Getlink();
        }
        if (p == NULL) {     //到邻接表末端仍然比v2小
            pt->Getlink() = new Edge(v2, weight, NULL);
            return 0;
        }
        if (p == Head[v1].Getadj() && p->GetVerAdj() > v2) {   //邻接表第一个就比v2大
            Edge* q = new Edge(v2, weight, NULL);
            q->Getlink() = p;
            Head[v1].Getadj() = q;
            return 0;
        }
        if (p->GetVerAdj() == v2) {
            return -4;    //插入边已存在  返回-4
        }
        if (p != Head[v1].Getadj() && p->GetVerAdj() > v2) {  //正常在邻接表中间添加的情况
            Edge* q = new Edge(v2, weight, NULL);
            pt->Getlink() = q;
            q->Getlink() = p;
            return 0;
        }
        return 0;
    }
    int DeleteEdge_Do(const int& v1, const int& v2) //删除边
    {
        Edge* p = Head[v1].Getadj();
        if (p == NULL)
        {
            return -1;   //返回-1表示所删除的边不存在
        }

        if (p->GetVerAdj() == v2)    //邻接表第一个就是v2的情况
        {
            Head[v1].Getadj() = p->Getlink();
            delete p;
            return 0;
        }
        Edge* pt = p;
        while (p != NULL)
        {
            if (p->GetVerAdj() == v2)
            {
                pt->Getlink() = p->Getlink();
                delete p;
                break;
            }
            pt = p;
            p = p->Getlink();
        }
        if (p == NULL)    //到邻接表末端仍未找到v2  则边不存在
        {
            return -1;   //返回-1表示所删除的边不存在
        }
        return 0;
    }
    int DeleteGraph_List()  //删除内容
    {
        for (int i = 0; i <= graphsize - 1; i++)
        {
            Edge* p = Head[i].Getadj();
            Edge* q = p;
            while (p != NULL)
            {
                p = p->Getlink();
                delete q;
                q = p;
            }
        }
        delete[] Head;
        delete map;
        return 0;
    }
public:
    int& GetMaxCost() 
    {
        return MaxCost;
    }
    Vertex*& GetHead()
    {
        return Head;
    }
    IMAGE*& GetMap()
    {
        return map;
    }
    int GetIndex(int marki)     //按代号查找在顶点表中的位置
    {
        for (int i = 0; i <= graphsize - 1; i++)
        {
            if (marki == Head[i].GetMark())
            {
                return i;
            }
        }
        return -1;   //找不到则返回-1
    }
    int GetIndex(string name)   //按名称查找在顶点表中的位置
    {
        for (int i = 0; i <= graphsize - 1; i++)
        {
            if (name == Head[i].GetVerName())
            {
                return i;
            }
        }
        return -1; //找不到则返回-1
    }
    int graph_con(fstream& file, char* mapname)                   //创建图
    {
        DeleteGraph_List();//先把类里面之前的内容删除掉
        map = new IMAGE;
        //void loadimage(IMAGE * pDstImg, LPCTSTR pImgFile, int nWidth = 0, int nHeight = 0, bool bResize = false);					// 从图片文件获取图像(bmp/gif/jpg/png/tif/emf/wmf/ico)
        //void loadimage(IMAGE * pDstImg, LPCTSTR pResType, LPCTSTR pResName, int nWidth = 0, int nHeight = 0, bool bResize = false);	// 从资源文件获取图像(bmp/gif/jpg/png/tif/emf/wmf/ico)

        loadimage(map, (LPCTSTR)mapname,0,0,false);
        file >> MaxCost; //从文件中提取最大值
        file >> graphsize;//从文件中提取当前结点数
        Head = new Vertex[graphsize];
        for (int i = 0; i <= graphsize - 1; i++)
        {
            file >> Head[i].GetVerName(); //从文件中提取结点名称
            file >> Head[i].GetDescribe(); //从文件中提取结点描述
            file >> Head[i].GetMark(); //从文件中提取数字标记
            file >> Head[i].GetX();
            Head[i].GetX() += LEFTBORDER;    //加左边界
            file >> Head[i].GetY(); 
            Head[i].GetY() += TOPBORDER;    //加上边界
            Head[i].Getadj() = NULL;  //相邻结点指针
        }
        int edgenum;  //边的数量
        file >> edgenum;
        for (int j = 0; j <= edgenum - 1; j++)
        {
            int v1, v2, weight;
            file >> v1;
            file >> v2;
            double dx = Head[GetIndex(v2)].GetX();
            double sx = Head[GetIndex(v1)].GetX();
            double dy = Head[GetIndex(v2)].GetY();
            double sy = Head[GetIndex(v1)].GetY();
            weight = (double)sqrt(((dx - sx) * (dx - sx) + (dy - sy) * (dy - sy)) / 30000.0) * 300.0;   //通过坐标计算边长度 赋给weight
            InsertEdge(GetIndex(v1), GetIndex(v2), weight);
        }
        return 0;
    }
    Graph_List()   //构造函数  构造一个空图  
    {
        Head = NULL;
        //map = NULL;
        map = new IMAGE;
        graphsize = 0;
        MaxCost = 0;
    }
    ~Graph_List() {
        DeleteGraph_List();
    }
    int SaveFile(char* filename) { //保存文件
        system("del filename ");   //删除文件
        ofstream fl(filename);//打开文件用于写，若文件不存在就创建它
        if (!fl)return -1;//打开文件失败则结束运行
        fl << GetMaxCost() << endl; //从文件中输出最大值
        fl << NumberOfVertices() << endl;//从文件中输出结点数
        for (int i = 0; i <= NumberOfVertices() - 1; i++) {
            fl << Head[i].GetVerName() << "\t\t\t" << Head[i].GetDescribe() << "\t\t\t\t\t\t" << Head[i].GetMark() << "\t" << Head[i].GetX() - LEFTBORDER << "\t" << Head[i].GetY() - TOPBORDER << endl;
        }       //用制表符对齐
        fl << endl;
        int numofedges = NumberOfEdges() * 2; //无向边*2=有向边
        fl << numofedges << endl;//输出边的总数
        int num = 0;
        for (int j = 0; j <= NumberOfVertices() - 1; j++) {
            Edge* p = Head[j].Getadj();
            while (p != NULL) {
                if (num == 5) {
                    fl << endl; //空行
                    num = 0;
                }
                fl << GetHead()[j].GetMark() << "\t" << GetHead()[p->GetVerAdj()].GetMark() << endl;
                p = p->Getlink();
                num++;
            }
        }
        fl.close();
        return 0;
    }
    bool GraphEmpty()const
    {
        return graphsize == 0;
    }
    int NumberOfVertices()const
    {
        return graphsize;
    }
    int NumberOfEdges()const
    {
        int n = 0;
        for (int i = 0; i <= graphsize - 1; i++)
        {
            Edge* p = Head[i].Getadj();
            while (p != NULL)
            {
                n++;
                p = p->Getlink();
            }
        }
        return n / 2;    //返回边的数目  不计重复边
    }
    int GetWeight(const int& v1, const int& v2) 
    {
        if (v1 > graphsize - 1 || v2 > graphsize - 1 || v1 < 0 || v2 < 0)
        {
            //cout << "查找元素超界！" << endl;
            return -1;
        }
        if (v1 == v2)      ///
        {
            return 0;
        }
        Edge* p = Head[v1].Getadj();
        while (p != NULL && p->GetVerAdj() != v2)
        {
            p = p->Getlink();
        }
        if (p != NULL)
        {
            return p->Getcost();
        }
        else
            return MaxCost + 1;   //两点之间没有边则返回 MaxCost + 1
    }
    void InsertVertex(string  name, string  describe, int mark, int x, int y) //插入结点
    {
        Vertex* head = new Vertex[graphsize + 1];   //创建一个新的顶点表  大小为现有大小再加1
        for (int i = 0; i <= graphsize - 1; i++)    //复制现有顶点表的内容到新顶点表
        {
            head[i] = Head[i];
        }
        head[graphsize].GetVerName() = name;    //把新添加的顶点加在新顶点表最后一个位置
        head[graphsize].GetDescribe() = describe;
        head[graphsize].GetMark() = mark;
        head[graphsize].GetX() = x;
        head[graphsize].GetY() = y;
        head[graphsize].Getadj() = NULL;
        delete[] Head;    //删除原顶点表
        Head = head;   //把Head指针指向新顶点表
        graphsize++;    //更新顶点数目
    }
    int InsertEdge(int v1, int  v2, int weight) //插入边
    {
        if (v1 > graphsize - 1 || v2 > graphsize - 1 || v1 < 0 || v2 < 0)
        {
            //cerr << "插入元素超界！无法完成操作！" << endl;
            return -1;     //插入元素超界返回-1
        }
        if (weight > MaxCost)
        {
            //cerr << "插入权值超出最大权值！无法完成操作！" << endl;
            return -2;            //返回-2表示权值过大
        }

        if (v1 == v2)
        {
            //cerr << "操作不合法" << endl;
            return -3;
        }
        int i = InsertEdge_Do(v1, v2, weight); //对照以前输入的边
        if (i == -4) {      //边已存在
            return i;
        }
        InsertEdge_Do(v2, v1, weight); //重新输入边
        return i;
    }
    int DeleteVertex(const int& v) //删除结点
    {
        if (v > graphsize - 1 || v < 0)
        {
            //cerr << "删除元素超界！" << endl;
            return -1;     //超界返回-1
        }
        for (int i = 0; i <= graphsize - 1; i++)
        {
            DeleteEdge(i, v);    //删除指向该顶点的所有边
        }
        for (int j = v; j <= graphsize - 2; j++)
        {
            Head[j] = Head[j + 1];    //把该顶点后边的元素向前挪一个位置
        }
        graphsize--;    //更新graphsize
        for (int k = 0; k <= graphsize - 1; k++)
        {
            Edge* p = Head[k].Getadj();
            while (p != NULL)
            {
                if (p->GetVerAdj() > v)
                {
                    p->GetVerAdj()--;    //所有边之中指向所删除顶点之后位置的  都要减1
                }
                p = p->Getlink();
            }
        }
        return 0;
    }
    int ModifyVertex(int v, string& name, string& describe, int& marki, int& x, int& y) { //修改结点元素
        if (v < 0 || v > NumberOfVertices() - 1) {
            return -1;
        }
        Head[v].GetVerName() = name;
        Head[v].GetDescribe() = describe;
        Head[v].GetMark() = marki;
        Head[v].GetX() = x;
        Head[v].GetX() += LEFTBORDER;
        Head[v].GetY() = y;
        Head[v].GetY() += TOPBORDER;
        return 0;
    }
    int DeleteEdge(int v2, int v1) //删除边
    {
        if (v1 > graphsize - 1 || v2 > graphsize - 1 || v1 < 0 || v2 < 0)
        {
            //cerr << "删除元素超界！" << endl;
            return -1;
        }
        if (v1 == v2)
        {
            return -1;
        }
        int i = DeleteEdge_Do(v1, v2);  //对比删除的边
        if (i == -1) { return i; }//删除的边不存在
        DeleteEdge_Do(v2, v1);//重新删除
        return i;
    }
    int DShortestPath(const int v, int*& path) //最短路径
    {
        int n = graphsize;
        int* s = new int[n]; //存放该顶点是否被访问过
        int* dist = new int[n]; //存放当前结点到其他结点的最短长度
        int i = 0, temp = 0;
        for (i = 0; i < n; i++)
        {
            s[i] = 0;//未访问结点为0
            dist[i] = MaxCost * graphsize * (graphsize - 1) / 2; //赋予相当于无穷的初值
            path[i] = -1;//路径数组
        }
        int u = v;//起点
        Edge* p = NULL;
        dist[u] = 0; //起点距离为0
        s[u] = 1;//当前结点被访问过
        path[u] = u;//起点路径指向自身
        for (i = 0; i < n; i++)
        {
            s[u] = 1;
            p = Head[u].Getadj();//获取邻边
            while (p != NULL)
            {
                temp = p->GetVerAdj();
                if (dist[u] + p->Getcost() < dist[temp]) //对比距离
                {
                    dist[temp] = dist[u] + p->Getcost();
                    path[temp] = u;
                }
                p = p->Getlink();
            }
            temp = MaxCost * graphsize * (graphsize - 1) / 2;;
            for (int j = 0; j < n; j++) //最短距离
            {
                if (s[j] == 0 && dist[j] < temp) 
                {
                    temp = dist[j];
                    u = j;//未访问中最小
                }
            }
        }
        delete[]s;
        delete[]dist;
        return 0;
    }
};
#endif