#ifndef  _Functions_h_ 
#define  _Functions_h_
#include<iostream>
#include<string>
#include<math.h>
#include <sstream>
using namespace std;
string int_to_str(int n) { //转换类型
    std::stringstream ss;
    std::string str;
    ss << n;
    ss >> str; 
    return str;
}
int str_to_num1(const char*& str) //转换类型
{
    if (!(str[0] == '-' || (str[0] >= '0' && str[0] <= '9'))) {
        return INT_MIN;
    }
    int n = 0;
    while (str[n] != '\0') {
        n++;
    }
    int number = 0;
    if (str[0] != '-') {
        for (int i = 0; i < n; i++) {
            if (!(str[i] >= '0' && str[i] <= '9')) {
                return INT_MIN;
            }
            number += (str[i] - '0') * pow(10, n - i - 1);
        }
    }
    if (str[0] == '-') {
        for (int i = 1; i < n; i++) {
            if (!(str[i] >= '0' && str[i] <= '9')) {
                return INT_MIN;
            }
            number += (str[i] - '0') * pow(10, n - i - 1);
        }
        number *= -1;
    }
    return number;
}
int str_to_num(const string& str) //转换类型
{
    const char* data = str.c_str();
    int number = str_to_num1(data);
    return number;
}
#endif#pragma once
